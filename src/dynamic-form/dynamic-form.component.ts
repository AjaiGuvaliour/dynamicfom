import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { debounceTime, distinctUntilChanged } from "rxjs/operators";
import { HttpClient } from '@angular/common/http';
import { delay } from 'rxjs/operators';
@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.css']
})
export class DynamicFormComponent implements OnInit {

  myForm: FormGroup;
  constructor(private http:HttpClient) { }
  @Input('formInputs') formInputs = [];
  ngOnInit() {
    this.myForm = this.dynamicFormGenerating();
  }

  get f() { return this.myForm.controls; }

  dynamicFormGenerating() {
    let form = {};
    for (let i = 0; i <= this.formInputs.length - 1; i++) {
      let input = this.formInputs[i];
      if (input['display']) {
        form[input['name']] = this.formValidation(input, true)
      }
      if(input['onLoadCall']){
        this.http.get(input['apiLink']).pipe(
          delay(2000)
        ).subscribe(ele=>{
          input['options']=ele;
        })
      }
    }
    return new FormGroup(form);
  }

  formValidation(input, formControlReq) {
    let validation = [];
    let inVal = input['validation'];
    inVal['required'] ? validation.push(Validators.required) : '';
    inVal['paternReq'] ? validation.push(Validators.pattern(inVal['pattern'])) : '';
    inVal['minReqLength'] ? validation.push(Validators.minLength(inVal['minLength'])) : '';
    inVal['maxReqLength'] ? validation.push(Validators.maxLength(inVal['maxLength'])) : '';
    inVal['maxValueBol'] ? validation.push(Validators.max(inVal['maxValue'])) : '';
    inVal['minValueBol'] ? validation.push(Validators.min(inVal['minValue'])) : '';
    let value = input.disabled ? { value: input.value, disabled: true } : input.value;
    return formControlReq ? new FormControl(value, validation) : validation;
  }

  formUpdating(item) {
    let value = this.myForm.get(item['name']).value;
    if (item['isProperty'] && value.length) {
      let ele = item['properties'][value];
      for (let i = 0; i <= ele.length - 1; i++) {
        let changes = ele[i]['changes'];
        let dataSet = this.formInputs.find(el => el.name == ele[i]['name']);
        dataSet['validation'] = changes['validation'] ? ele[i]['validation'] : dataSet['validation'];
        dataSet['disabled'] = changes['disabled'] ? ele[i]['disabled'] : dataSet['disabled'];
        dataSet['type'] = changes['type'] ? ele[i]['type'] : dataSet['type'];
        dataSet['display'] = changes['display'] ? ele[i]['display'] : dataSet['display'];
        if (changes['display']) {
          if (ele[i]['display']) {
            let validation = this.formValidation(dataSet, true);
            if (!Array.isArray(validation)) {
              this.myForm.addControl(dataSet['name'], validation)
            }
          } else {
            this.myForm.removeControl(dataSet['name']);
          }
        } else {
          if (changes['disabled']) {
            if (dataSet['disabled']) {
              this.myForm.controls[dataSet['name']].disable();
            } else {
              this.myForm.controls[dataSet['name']].enable();
            }
          }
          if (changes['validation']) {
            let validation = this.formValidation(dataSet, false);
            if (Array.isArray(validation)) {
              this.myForm.controls[dataSet['name']].setValidators(validation);
              this.myForm.controls[dataSet['name']].updateValueAndValidity();
            }
          }
        }
      }
    }
    if(item['rebindFields'].length){
     this.rebindApiCall(item['rebindFields']);
    }
  }

  rebindApiCall(items){
    for(let i= 0; i<=items.length-1;i++){
       let item = items[i];
       if(item['type']=='call'){

       }else{
        this.myForm.patchValue({[item['name']]:''});
       }
    }
  }

  submit() {
    console.log(this.myForm.getRawValue(), this.formInputs)
  }

}
