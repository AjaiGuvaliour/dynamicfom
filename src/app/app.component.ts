import { Component, ViewContainerRef, ElementRef, ViewChild, AfterViewInit, OnInit, ViewChildren, ChangeDetectorRef, ContentChild } from '@angular/core';
import { Subject } from 'rxjs';
import {formDetails} from '../dynamicForm'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {

  formInputs:any = [];

  constructor() { 
  }

  ngOnInit(): void {
   this.formInputs= JSON.parse(JSON.stringify(formDetails));
  }
 
  ngAfterViewInit() {
  }
}


