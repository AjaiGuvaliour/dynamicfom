import { environment } from './environments/environment.ts'
export const formDetails = [
    {
        "name": "id",
        "type": "number",
        "validation": {
            "required": false,
            "paternReq": false,
            "pattern": "",
            "minReqLength": false,
            "minLength": 0,
            "maxReqLength": false,
            "maxLength": 0,
            "minValueBol": false,
            "minValue": 0,
            "maxValue": 0,
            "maxValueBol": false
        },
        "displayName": "Id",
        "options": [],
        isProperty: false,
        rebindFields: [],
        properties: {},
        "onLoadCall": false,
        "apiLink": "",
        "class": "col-xl-6 col-md-6 col-xs-12 col-sm-12 col-lg-6",
        "display": true,
        "value": 3,
        "disabled": true,
    },
    {
        "name": "catalogId",
        "type": "number",
        "validation": {
            "required": true,
            "paternReq": false,
            "pattern": "",
            "minReqLength": false,
            "minLength": 0,
            "maxReqLength": false,
            "maxLength": 4,
            "minValueBol": true,
            "minValue": 0,
            "maxValue": 20000,
            "maxValueBol": true
        },
        "displayName": "Catalog Id",
        "options": [],
        isProperty: false,
        rebindFields: [],
        properties: {},
        "onLoadCall": false,
        "apiLink": "",
        "class": "col-xl-6 col-md-6 col-xs-12 col-sm-12 col-lg-6",
        "display": true,
        "value": "",
        "disabled": false,
    },
    {
        "name": "email",
        "type": "text",
        "validation": {
            "required": true,
            "paternReq": true,
            "pattern": "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$",
            "minReqLength": false,
            "minLength": 0,
            "maxReqLength": false,
            "maxLength": 0,
            "minValueBol": false,
            "minValue": 0,
            "maxValue": 0,
            "maxValueBol": false
        },
        "displayName": "Email",
        "options": [],
        "onLoadCall": false,
        "apiLink": "",
        isProperty: false,
        rebindFields: [],
        properties: {},
        "class": "col-xl-12 col-md-12 col-xs-12 col-sm-12 col-lg-12",
        "display": true,
        "value": "",
        "disabled": false,
    },
    {
        "name": "catId",
        "type": "text",
        "validation": {
            "required": true,
            "paternReq": false,
            "pattern": "",
            "minReqLength": true,
            "minLength": 0,
            "maxReqLength": true,
            "maxLength": 4,
            "minValueBol": false,
            "minValue": 0,
            "maxValue": 0,
            "maxValueBol": false
        },
        "displayName": "Cat Id",
        "options": [],
        "onLoadCall": false,
        "apiLink": "",
        isProperty: false,
        rebindFields: [],
        properties: {},
        "class": "col-xl-6 col-md-6 col-xs-12 col-sm-12 col-lg-6",
        "display": true,
        "value": "",
        "disabled": false,
    },
    {
        "name": "catalogName",
        "type": "text",
        "validation": {
            "required": true,
            "paternReq": false,
            "pattern": "",
            "minReqLength": false,
            "minLength": 0,
            "maxReqLength": false,
            "maxLength": 0,
            "minValueBol": false,
            "minValue": 0,
            "maxValue": 0,
            "maxValueBol": false
        },
        "displayName": "Catalog Name",
        "options": [],
        "onLoadCall": false,
        "apiLink": "",
        isProperty: false,
        rebindFields: [],
        properties: {},
        "class": "col-xl-6 col-md-6 col-xs-12 col-sm-12 col-lg-6",
        "display": true,
        "value": "",
        "disabled": false
    },
    {
        "name": "catalogStatus",
        "type": "select",
        "onLoadCall": true,
        "apiLink": environment.api1,
        "validation": {
            "required": true,
            "paternReq": false,
            "pattern": "",
            "minReqLength": false,
            "minLength": 0,
            "maxReqLength": false,
            "maxLength": 0,
            "minValueBol": false,
            "minValue": 0,
            "maxValue": 0,
            "maxValueBol": false
        },
        "displayName": "Catalog Status",
        "options": [],
        isProperty: true,
        rebindFields: [],
        properties: {
            "active": [
                {
                    "name": "dealName",
                    "changes": {
                        "type": false,
                        "display": true,
                        "disabled": false,
                        "validation": true
                    },
                    "validation": {
                        "required": true,
                        "paternReq": false,
                        "pattern": "",
                        "minReqLength": true,
                        "minLength": 4,
                        "maxReqLength": false,
                        "maxLength": 0,
                        "minValueBol": false,
                        "minValue": 0,
                        "maxValue": 0,
                        "maxValueBol": false
                    },
                    "display": true,
                    "options": [],
                    "value": "",
                },
                {
                    "name": "id",
                    "changes": {
                        "type": true,
                        "display": false,
                        "disabled": true,
                        "validation": true
                    },
                    "validation": {
                        "required": true,
                        "paternReq": false,
                        "pattern": "",
                        "minReqLength": false,
                        "minLength": 0,
                        "maxReqLength": false,
                        "maxLength": 0,
                        "minValueBol": false,
                        "minValue": 0,
                        "maxValue": 0,
                        "maxValueBol": false
                    },
                    "type": "date",
                    "disabled": false,
                    "options": [],
                    "value": ""
                }
            ],
            "inActive": [
                {
                    "name": "dealName",
                    "changes": {
                        "type": false,
                        "display": true,
                        "disabled": false,
                        "validation": false
                    },
                    "display": false,
                },
                {
                    "name": "id",
                    "changes": {
                        "type": true,
                        "display": false,
                        "disabled": true,
                        "validation": false
                    },
                    "type": "text",
                    "validation": {},
                    "display": false,
                    "disabled": true,
                    "options": [],
                    "value": "",
                }
            ]
        },
        "display": true,
        "class": "col-xl-6 col-md-6 col-xs-12 col-sm-12 col-lg-6",
        "value": "inActive",
        "disabled": false
    },
    {
        "name": "dealName",
        "type": "text",
        "onLoadCall": false,
        "apiLink": "",
        "validation": {
            "required": false,
            "paternReq": false,
            "pattern": "",
            "minReqLength": false,
            "minLength": 0,
            "maxReqLength": false,
            "maxLength": 0,
            "minValueBol": false,
            "minValue": 0,
            "maxValue": 0,
            "maxValueBol": false
        },
        "displayName": "Deal Name",
        "options": [],
        isProperty: false,
        rebindFields: [],
        properties: {},
        "class": "col-xl-6 col-md-6 col-xs-12 col-sm-12 col-lg-6",
        "display": false,
        "value": "",
        "disabled": false
    }
]